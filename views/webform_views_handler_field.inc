<?php

class webform_views_handler_field extends views_handler_field {

  /**
   * Do nothing -- to override the parent query.
   *
  function query() {

  } // query() */


  /**
   * Add webform component specific options.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['alter']['contains']['key2label'] = array('default' => 1);
    $options['separator'] = array('default' => ', ');

    return $options;

  } // option_definition() */


  /**
   * Add webform component specific options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    if (@$this->definition['webform']['multiple']) {
      $form['separator'] = array(
        '#type' => 'textfield',
        '#title' => t('Multi value separator'),
        '#description' => t('If this is a multivalue component, this string will be used as separator. <strong>Can contain HTML.</strong>'),
        '#default_value' => $this->options['separator'],
      );
    }

    if ('select' == @$this->definition['webform']['type']) {
      $form['alter']['key2label'] = array(
        '#type' => 'checkbox',
        '#title' => t('Convert option key to label'),
        '#description' => t('Only the key is stored in the database. The label is stored in the webform component. Check this box to display the label instead of the key.'),
        '#default_value' => $this->options['alter']['key2label'],
      );
    }

  } // options_form() */


  /**
   * Potentially use webform specific rendering. Otherwise, use normal rendering.
   */
  function render($values) {
    $options = (object) $this->definition['webform'];

    $value = $this->get_value($values);
    $sanitize = TRUE;

    // Special render: webform option key -> label.
    if ('select' == $options->type && isset($options->options) && $this->options['alter']['key2label']) {
      $value = $this->keysToLabels($value, $options->options);
    }

    // Special render: multiple values.
    if (is_array($value)) {
      $separator = isset($this->options['separator']) ? $this->options['separator'] : ', ';
      $value = array_map(array($this, 'sanitize_value'), $value);
      $sanitize = FALSE;
      $value = implode($separator, $value);
    }

    // Probably sanitize. Not multiples though.
    if ($sanitize) {
      $value = $this->sanitize_value($value);
    }

    // Normal, literal render.
    return $value;

  } // render() */


  /**
   * 
   */
  function keysToLabels($keys, $labels) {
    $single = !is_array($keys);
    $keys = (array) $keys;

    foreach ($keys as $i => $key) {
      if (isset($labels[$key])) {
        $keys[$i] = $labels[$key];
      }
    }

    return $single ? $keys[0] : $keys;
  }

}
